from dash import Dash, html, dcc, callback, Output, Input
import plotly.express as px
import pandas as pd
import dash_bootstrap_components as dbc

df = pd.read_csv('data.csv')
filtered_df = df.replace(-2, 0).replace(-1, 0).groupby(["Player", "Team"]).sum()

app = Dash(use_pages=False, external_stylesheets=[dbc.themes.BOOTSTRAP, dbc.icons.FONT_AWESOME])

app.layout = html.Div([
    html.H1(children='Статистика по игроку', style={'textAlign':'center'}),
    dcc.Dropdown(df.Player.unique(), 'Canada', id='dropdown-selection'),
    html.H2(children='Голы', style={'textAlign':'center'}),
    dcc.Graph(id='graph-content-goals'),
    html.H2(children='Передачи', style={'textAlign':'center'}),
    dcc.Graph(id='graph-content-assist'),
    html.H2(children='Очки', style={'textAlign':'center'}),
    dcc.Graph(id='graph-content-score'),
    html.H2(children='Фолы', style={'textAlign':'center'}),
    dcc.Graph(id='graph-content-falls')
])

@callback(
    Output('graph-content-goals', 'figure'),
    Input('dropdown-selection', 'value')
)
def update_graph_goals(value):
    dff = df[df.Player==value]
    tmp_score = dff.replace(-2, 0).replace(-1, 0).loc[:, 'Goal':'Mark'].cumsum()
    tmp_score['Team'] = dff['Team']
    return px.line(tmp_score, x='Team', y='Goal')

@callback(
    Output('graph-content-assist', 'figure'),
    Input('dropdown-selection', 'value')
)
def update_graph_assist(value):
    dff = df[df.Player==value]
    tmp_score = dff.replace(-2, 0).replace(-1, 0).loc[:, 'Goal':'Mark'].cumsum()
    tmp_score['Team'] = dff['Team']
    return px.line(tmp_score, x='Team', y='Assist')

@callback(
    Output('graph-content-score', 'figure'),
    Input('dropdown-selection', 'value')
)
def update_graph_score(value):
    dff = df[df.Player==value]
    tmp_score = dff.replace(-2, 0).replace(-1, 0).loc[:, 'Goal':'Mark'].cumsum()
    tmp_score['Team'] = dff['Team']
    return px.line(tmp_score, x='Team', y='Score')

@callback(
    Output('graph-content-falls', 'figure'),
    Input('dropdown-selection', 'value')
)
def update_graph_falls(value):
    dff = df[df.Player==value]
    tmp_score = dff.replace(-2, 0).replace(-1, 0).loc[:, 'Goal':'Mark'].cumsum()
    tmp_score['Team'] = dff['Team']
    return px.line(tmp_score, x='Team', y='Falls')

if __name__ == '__main__':

    app.run(debug=True)